﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This experiment was created using PsychoPy3 Experiment Builder (v2021.1.2),
    on February 13, 2024, at 15:57
If you publish work using this script the most relevant publication is:

    Peirce J, Gray JR, Simpson S, MacAskill M, Höchenberger R, Sogo H, Kastman E, Lindeløv JK. (2019) 
        PsychoPy2: Experiments in behavior made easy Behav Res 51: 195. 
        https://doi.org/10.3758/s13428-018-01193-y

"""

from __future__ import absolute_import, division

from psychopy import locale_setup
from psychopy import prefs
from psychopy import sound, gui, visual, core, data, event, logging, clock, colors
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER)

import numpy as np  # whole numpy lib is available, prepend 'np.'
from numpy import (sin, cos, tan, log, log10, pi, average,
                   sqrt, std, deg2rad, rad2deg, linspace, asarray)
from numpy.random import random, randint, normal, shuffle, choice as randchoice
import os  # handy system and path functions
import sys  # to get file system encoding

from psychopy.hardware import keyboard



# Ensure that relative paths start from the same directory as this script
_thisDir = os.path.dirname(os.path.abspath(__file__))
os.chdir(_thisDir)

# Store info about the experiment session
psychopyVersion = '2021.1.2'
expName = 'rtt'  # from the Builder filename that created this script
expInfo = {'participant': '', 'session': '', 'age': '', 'gender': ''}
dlg = gui.DlgFromDict(dictionary=expInfo, sortKeys=False, title=expName)
if dlg.OK == False:
    core.quit()  # user pressed cancel
expInfo['date'] = data.getDateStr()  # add a simple timestamp
expInfo['expName'] = expName
expInfo['psychopyVersion'] = psychopyVersion

# Data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
filename = _thisDir + os.sep + u'data/%s_%s_%s' % (expInfo['participant'], expName, expInfo['date'])

# An ExperimentHandler isn't essential but helps with data saving
thisExp = data.ExperimentHandler(name=expName, version='',
    extraInfo=expInfo, runtimeInfo=None,
    originPath='\\\\wsl.localhost\\Ubuntu-20.04\\home\\amartin\\choice_rtt\\code\\experiment\\rtt_lastrun.py',
    savePickle=True, saveWideText=True,
    dataFileName=filename)
# save a log file for detail verbose info
logFile = logging.LogFile(filename+'.log', level=logging.EXP)
logging.console.setLevel(logging.WARNING)  # this outputs to the screen, not a file

endExpNow = False  # flag for 'escape' or other condition => quit the exp
frameTolerance = 0.001  # how close to onset before 'same' frame

# Start Code - component code to be run after the window creation

# Setup the Window
win = visual.Window(
    size=[1536, 864], fullscr=True, screen=0, 
    winType='pyglet', allowGUI=False, allowStencil=False,
    monitor='testMonitor', color=[0,0,0], colorSpace='rgb',
    blendMode='avg', useFBO=True, 
    units='height')
# store frame rate of monitor if we can measure it
expInfo['frameRate'] = win.getActualFrameRate()
if expInfo['frameRate'] != None:
    frameDur = 1.0 / round(expInfo['frameRate'])
else:
    frameDur = 1.0 / 60.0  # could not measure, so guess

# create a default keyboard (e.g. to check for escape)
defaultKeyboard = keyboard.Keyboard()

# Initialize components for Routine "welcome"
welcomeClock = core.Clock()
text_3 = visual.TextStim(win=win, name='text_3',
    text='Welcome\n\nPress space to continue',
    font='Open Sans',
    pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
key_resp_3 = keyboard.Keyboard()

# Initialize components for Routine "instruction"
instructionClock = core.Clock()
text = visual.TextStim(win=win, name='text',
    text='In this task, you will make decisions as to which stimulus you have seen. There are three versions of the task. In the first, you have to decide which shape you have seen, in the second which image and the third which sound you’ve heard. Before each task, you will get set of specific instructions and short practice period. Please press the space bar to continue',
    font='Open Sans',
    pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
key_resp = keyboard.Keyboard()

# Initialize components for Routine "instruct_shape"
instruct_shapeClock = core.Clock()
text_4 = visual.TextStim(win=win, name='text_4',
    text='In this task you will make a decision as to which shape you have seen.\n\nPress C or click cross for a cross Press V or click square for a square Press B or click plus for a plus\n\nFirst, we will have a quick practice.\n\nPush space bar to begin.',
    font='Open Sans',
    pos=(0, 0.1), height=0.035, wrapWidth=None, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
key_resp_4 = keyboard.Keyboard()
image = visual.ImageStim(
    win=win,
    name='image', 
    image='C:\\\\Users\\\\aimee\\\\Downloads\\\\nowa\\\\nowaschool-main-school-materials\\\\nowaschool-main-school-materials\\\\school\\\\materials\\\\psychopy\\\\choice_rtt\\\\stimuli\\\\shapes\\\\response_square.jpg', mask=None,
    ori=0.0, pos=(0, -0.25), size=(0.2, 0.1),
    color=[1,1,1], colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=128.0, interpolate=True, depth=-2.0)
image_2 = visual.ImageStim(
    win=win,
    name='image_2', 
    image='C:\\\\Users\\\\aimee\\\\Downloads\\\\nowa\\\\nowaschool-main-school-materials\\\\nowaschool-main-school-materials\\\\school\\\\materials\\\\psychopy\\\\choice_rtt\\\\stimuli\\\\shapes\\\\response_plus.jpg', mask=None,
    ori=0.0, pos=(0.25, -0.25), size=(0.2, 0.1),
    color=[1,1,1], colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=128.0, interpolate=True, depth=-3.0)
image_3 = visual.ImageStim(
    win=win,
    name='image_3', 
    image='C:\\\\Users\\\\aimee\\\\Downloads\\\\nowa\\\\nowaschool-main-school-materials\\\\nowaschool-main-school-materials\\\\school\\\\materials\\\\psychopy\\\\choice_rtt\\\\stimuli\\\\shapes\\\\response_cross.jpg', mask=None,
    ori=0.0, pos=(-0.25, -0.25), size=(0.2, 0.1),
    color=[1,1,1], colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=128.0, interpolate=True, depth=-4.0)

# Initialize components for Routine "trial"
trialClock = core.Clock()
square = visual.ImageStim(
    win=win,
    name='square', 
    image='C:\\\\Users\\\\aimee\\\\Downloads\\\\nowa\\\\nowaschool-main-school-materials\\\\nowaschool-main-school-materials\\\\school\\\\materials\\\\psychopy\\\\choice_rtt\\\\stimuli\\\\shapes\\\\response_square.jpg', mask=None,
    ori=0.0, pos=(0, -0.25), size=(0.2, 0.1),
    color=[1,1,1], colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=128.0, interpolate=True, depth=-1.0)
plus = visual.ImageStim(
    win=win,
    name='plus', 
    image='C:\\\\Users\\\\aimee\\\\Downloads\\\\nowa\\\\nowaschool-main-school-materials\\\\nowaschool-main-school-materials\\\\school\\\\materials\\\\psychopy\\\\choice_rtt\\\\stimuli\\\\shapes\\\\response_plus.jpg', mask=None,
    ori=0.0, pos=(0.25, -0.25), size=(0.2, 0.1),
    color=[1,1,1], colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=128.0, interpolate=True, depth=-2.0)
cross = visual.ImageStim(
    win=win,
    name='cross', 
    image='C:\\\\Users\\\\aimee\\\\Downloads\\\\nowa\\\\nowaschool-main-school-materials\\\\nowaschool-main-school-materials\\\\school\\\\materials\\\\psychopy\\\\choice_rtt\\\\stimuli\\\\shapes\\\\response_cross.jpg', mask=None,
    ori=0.0, pos=(-0.25, -0.25), size=(0.2, 0.1),
    color=[1,1,1], colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=128.0, interpolate=True, depth=-3.0)
left_far_tile = visual.ImageStim(
    win=win,
    name='left_far_tile', 
    image='C:\\\\Users\\\\aimee\\\\Downloads\\\\nowa\\\\nowaschool-main-school-materials\\\\nowaschool-main-school-materials\\\\school\\\\materials\\\\psychopy\\\\choice_rtt\\\\stimuli\\\\shapes\\\\white_square.png', mask=None,
    ori=0.0, pos=(-0.375, 0), size=(0.22, 0.22),
    color=[1,1,1], colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=128.0, interpolate=True, depth=-4.0)
left_mid_tile = visual.ImageStim(
    win=win,
    name='left_mid_tile', 
    image='C:\\\\Users\\\\aimee\\\\Downloads\\\\nowa\\\\nowaschool-main-school-materials\\\\nowaschool-main-school-materials\\\\school\\\\materials\\\\psychopy\\\\choice_rtt\\\\stimuli\\\\shapes\\\\white_square.png', mask=None,
    ori=0.0, pos=(-0.125, 0), size=(0.22, 0.22),
    color=[1,1,1], colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=128.0, interpolate=True, depth=-5.0)
right_mid_tile = visual.ImageStim(
    win=win,
    name='right_mid_tile', 
    image='C:\\\\Users\\\\aimee\\\\Downloads\\\\nowa\\\\nowaschool-main-school-materials\\\\nowaschool-main-school-materials\\\\school\\\\materials\\\\psychopy\\\\choice_rtt\\\\stimuli\\\\shapes\\\\white_square.png', mask=None,
    ori=0.0, pos=(0.125, 0), size=(0.22, 0.22),
    color=[1,1,1], colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=128.0, interpolate=True, depth=-6.0)
right_far_tile = visual.ImageStim(
    win=win,
    name='right_far_tile', 
    image='C:\\\\Users\\\\aimee\\\\Downloads\\\\nowa\\\\nowaschool-main-school-materials\\\\nowaschool-main-school-materials\\\\school\\\\materials\\\\psychopy\\\\choice_rtt\\\\stimuli\\\\shapes\\\\white_square.png', mask=None,
    ori=0.0, pos=(0.375, 0), size=(0.22, 0.22),
    color=[1,1,1], colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=128.0, interpolate=True, depth=-7.0)
target_image = visual.ImageStim(
    win=win,
    name='target_image', 
    image='sin', mask=None,
    ori=0.0, pos=[0,0], size=(0.2, 0.2),
    color=[1,1,1], colorSpace='rgb', opacity=None,
    flipHoriz=False, flipVert=False,
    texRes=128.0, interpolate=True, depth=-8.0)
keyboard_response = keyboard.Keyboard()

# Initialize components for Routine "end_of_exp"
end_of_expClock = core.Clock()
text_2 = visual.TextStim(win=win, name='text_2',
    text='This is the end of the experiment. \n\nThanks for participating. \n\nPress space to end. ',
    font='Open Sans',
    pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
    color='white', colorSpace='rgb', opacity=None, 
    languageStyle='LTR',
    depth=0.0);
key_resp_2 = keyboard.Keyboard()

# Create some handy timers
globalClock = core.Clock()  # to track the time since experiment started
routineTimer = core.CountdownTimer()  # to track time remaining of each (non-slip) routine 

# ------Prepare to start Routine "welcome"-------
continueRoutine = True
# update component parameters for each repeat
key_resp_3.keys = []
key_resp_3.rt = []
_key_resp_3_allKeys = []
# keep track of which components have finished
welcomeComponents = [text_3, key_resp_3]
for thisComponent in welcomeComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
welcomeClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "welcome"-------
while continueRoutine:
    # get current time
    t = welcomeClock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=welcomeClock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *text_3* updates
    if text_3.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        text_3.frameNStart = frameN  # exact frame index
        text_3.tStart = t  # local t and not account for scr refresh
        text_3.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(text_3, 'tStartRefresh')  # time at next scr refresh
        text_3.setAutoDraw(True)
    
    # *key_resp_3* updates
    waitOnFlip = False
    if key_resp_3.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        key_resp_3.frameNStart = frameN  # exact frame index
        key_resp_3.tStart = t  # local t and not account for scr refresh
        key_resp_3.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(key_resp_3, 'tStartRefresh')  # time at next scr refresh
        key_resp_3.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(key_resp_3.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(key_resp_3.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if key_resp_3.status == STARTED and not waitOnFlip:
        theseKeys = key_resp_3.getKeys(keyList=['space'], waitRelease=False)
        _key_resp_3_allKeys.extend(theseKeys)
        if len(_key_resp_3_allKeys):
            key_resp_3.keys = _key_resp_3_allKeys[-1].name  # just the last key pressed
            key_resp_3.rt = _key_resp_3_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in welcomeComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "welcome"-------
for thisComponent in welcomeComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('text_3.started', text_3.tStartRefresh)
thisExp.addData('text_3.stopped', text_3.tStopRefresh)
# check responses
if key_resp_3.keys in ['', [], None]:  # No response was made
    key_resp_3.keys = None
thisExp.addData('key_resp_3.keys',key_resp_3.keys)
if key_resp_3.keys != None:  # we had a response
    thisExp.addData('key_resp_3.rt', key_resp_3.rt)
thisExp.addData('key_resp_3.started', key_resp_3.tStartRefresh)
thisExp.addData('key_resp_3.stopped', key_resp_3.tStopRefresh)
thisExp.nextEntry()
# the Routine "welcome" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# ------Prepare to start Routine "instruction"-------
continueRoutine = True
# update component parameters for each repeat
key_resp.keys = []
key_resp.rt = []
_key_resp_allKeys = []
# keep track of which components have finished
instructionComponents = [text, key_resp]
for thisComponent in instructionComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
instructionClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "instruction"-------
while continueRoutine:
    # get current time
    t = instructionClock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=instructionClock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *text* updates
    if text.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        text.frameNStart = frameN  # exact frame index
        text.tStart = t  # local t and not account for scr refresh
        text.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(text, 'tStartRefresh')  # time at next scr refresh
        text.setAutoDraw(True)
    
    # *key_resp* updates
    waitOnFlip = False
    if key_resp.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        key_resp.frameNStart = frameN  # exact frame index
        key_resp.tStart = t  # local t and not account for scr refresh
        key_resp.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(key_resp, 'tStartRefresh')  # time at next scr refresh
        key_resp.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(key_resp.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(key_resp.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if key_resp.status == STARTED and not waitOnFlip:
        theseKeys = key_resp.getKeys(keyList=['space'], waitRelease=False)
        _key_resp_allKeys.extend(theseKeys)
        if len(_key_resp_allKeys):
            key_resp.keys = _key_resp_allKeys[-1].name  # just the last key pressed
            key_resp.rt = _key_resp_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in instructionComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "instruction"-------
for thisComponent in instructionComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('text.started', text.tStartRefresh)
thisExp.addData('text.stopped', text.tStopRefresh)
# check responses
if key_resp.keys in ['', [], None]:  # No response was made
    key_resp.keys = None
thisExp.addData('key_resp.keys',key_resp.keys)
if key_resp.keys != None:  # we had a response
    thisExp.addData('key_resp.rt', key_resp.rt)
thisExp.addData('key_resp.started', key_resp.tStartRefresh)
thisExp.addData('key_resp.stopped', key_resp.tStopRefresh)
thisExp.nextEntry()
# the Routine "instruction" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# set up handler to look after randomisation of conditions etc
trials = data.TrialHandler(nReps=1.0, method='random', 
    extraInfo=expInfo, originPath=-1,
    trialList=data.importConditions('C:Users\\\\aimee\\\\Downloads\\\\nowa\\\\shapes_targets.xlsx'),
    seed=None, name='trials')
thisExp.addLoop(trials)  # add the loop to the experiment
thisTrial = trials.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisTrial.rgb)
if thisTrial != None:
    for paramName in thisTrial:
        exec('{} = thisTrial[paramName]'.format(paramName))

for thisTrial in trials:
    currentLoop = trials
    # abbreviate parameter names if possible (e.g. rgb = thisTrial.rgb)
    if thisTrial != None:
        for paramName in thisTrial:
            exec('{} = thisTrial[paramName]'.format(paramName))
    
    # ------Prepare to start Routine "instruct_shape"-------
    continueRoutine = True
    # update component parameters for each repeat
    key_resp_4.keys = []
    key_resp_4.rt = []
    _key_resp_4_allKeys = []
    # keep track of which components have finished
    instruct_shapeComponents = [text_4, key_resp_4, image, image_2, image_3]
    for thisComponent in instruct_shapeComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    instruct_shapeClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "instruct_shape"-------
    while continueRoutine:
        # get current time
        t = instruct_shapeClock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=instruct_shapeClock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *text_4* updates
        if text_4.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            text_4.frameNStart = frameN  # exact frame index
            text_4.tStart = t  # local t and not account for scr refresh
            text_4.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(text_4, 'tStartRefresh')  # time at next scr refresh
            text_4.setAutoDraw(True)
        
        # *key_resp_4* updates
        waitOnFlip = False
        if key_resp_4.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            key_resp_4.frameNStart = frameN  # exact frame index
            key_resp_4.tStart = t  # local t and not account for scr refresh
            key_resp_4.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(key_resp_4, 'tStartRefresh')  # time at next scr refresh
            key_resp_4.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(key_resp_4.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(key_resp_4.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if key_resp_4.status == STARTED and not waitOnFlip:
            theseKeys = key_resp_4.getKeys(keyList=['space'], waitRelease=False)
            _key_resp_4_allKeys.extend(theseKeys)
            if len(_key_resp_4_allKeys):
                key_resp_4.keys = _key_resp_4_allKeys[-1].name  # just the last key pressed
                key_resp_4.rt = _key_resp_4_allKeys[-1].rt
                # a response ends the routine
                continueRoutine = False
        
        # *image* updates
        if image.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            image.frameNStart = frameN  # exact frame index
            image.tStart = t  # local t and not account for scr refresh
            image.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(image, 'tStartRefresh')  # time at next scr refresh
            image.setAutoDraw(True)
        
        # *image_2* updates
        if image_2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            image_2.frameNStart = frameN  # exact frame index
            image_2.tStart = t  # local t and not account for scr refresh
            image_2.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(image_2, 'tStartRefresh')  # time at next scr refresh
            image_2.setAutoDraw(True)
        
        # *image_3* updates
        if image_3.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            image_3.frameNStart = frameN  # exact frame index
            image_3.tStart = t  # local t and not account for scr refresh
            image_3.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(image_3, 'tStartRefresh')  # time at next scr refresh
            image_3.setAutoDraw(True)
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in instruct_shapeComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "instruct_shape"-------
    for thisComponent in instruct_shapeComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    trials.addData('text_4.started', text_4.tStartRefresh)
    trials.addData('text_4.stopped', text_4.tStopRefresh)
    # check responses
    if key_resp_4.keys in ['', [], None]:  # No response was made
        key_resp_4.keys = None
    trials.addData('key_resp_4.keys',key_resp_4.keys)
    if key_resp_4.keys != None:  # we had a response
        trials.addData('key_resp_4.rt', key_resp_4.rt)
    trials.addData('key_resp_4.started', key_resp_4.tStartRefresh)
    trials.addData('key_resp_4.stopped', key_resp_4.tStopRefresh)
    trials.addData('image.started', image.tStartRefresh)
    trials.addData('image.stopped', image.tStopRefresh)
    trials.addData('image_2.started', image_2.tStartRefresh)
    trials.addData('image_2.stopped', image_2.tStopRefresh)
    trials.addData('image_3.started', image_3.tStartRefresh)
    trials.addData('image_3.stopped', image_3.tStopRefresh)
    # the Routine "instruct_shape" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # ------Prepare to start Routine "trial"-------
    continueRoutine = True
    # update component parameters for each repeat
    list_onsets = [1, 1.2, 1.4, 1.6, 1.8]
    shuffle(list_onsets)
    onset_trial = list_onsets[0]
    list_positions = [-0.375, -0.125, 0.125, 0.375]
    shuffle(list_positions)
    position_trial = list_positions[0]
    
    #corrans below
    
    if TargetImage == 'C:Users\aimee\Downloads\nowa\nowaschool-main-school-materials\nowaschool-main-school-materials\school\materials\psychopy\choice_rtt\stimuli\shapes\target_square.jpg': 
        corrAns = 'v'
    elif TargetImage == 'C:Users\aimee\Downloads\nowa\nowaschool-main-school-materials\nowaschool-main-school-materials\school\materials\psychopy\choice_rtt\stimuli\shapes\target_cross.jpg':
        corrAns = 'c' 
    elif TargetImage == 'C:Users\aimee\Downloads\nowa\nowaschool-main-school-materials\nowaschool-main-school-materials\school\materials\psychopy\choice_rtt\stimuli\shapes\target_plus.jpg':
        corrAns = 'b' 
    target_image.setPos((position_trial, 0))
    target_image.setImage(TargetImage)
    keyboard_response.keys = []
    keyboard_response.rt = []
    _keyboard_response_allKeys = []
    # keep track of which components have finished
    trialComponents = [square, plus, cross, left_far_tile, left_mid_tile, right_mid_tile, right_far_tile, target_image, keyboard_response]
    for thisComponent in trialComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    trialClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "trial"-------
    while continueRoutine:
        # get current time
        t = trialClock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=trialClock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *square* updates
        if square.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            square.frameNStart = frameN  # exact frame index
            square.tStart = t  # local t and not account for scr refresh
            square.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(square, 'tStartRefresh')  # time at next scr refresh
            square.setAutoDraw(True)
        
        # *plus* updates
        if plus.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            plus.frameNStart = frameN  # exact frame index
            plus.tStart = t  # local t and not account for scr refresh
            plus.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(plus, 'tStartRefresh')  # time at next scr refresh
            plus.setAutoDraw(True)
        
        # *cross* updates
        if cross.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            cross.frameNStart = frameN  # exact frame index
            cross.tStart = t  # local t and not account for scr refresh
            cross.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(cross, 'tStartRefresh')  # time at next scr refresh
            cross.setAutoDraw(True)
        
        # *left_far_tile* updates
        if left_far_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
            # keep track of start time/frame for later
            left_far_tile.frameNStart = frameN  # exact frame index
            left_far_tile.tStart = t  # local t and not account for scr refresh
            left_far_tile.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(left_far_tile, 'tStartRefresh')  # time at next scr refresh
            left_far_tile.setAutoDraw(True)
        
        # *left_mid_tile* updates
        if left_mid_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
            # keep track of start time/frame for later
            left_mid_tile.frameNStart = frameN  # exact frame index
            left_mid_tile.tStart = t  # local t and not account for scr refresh
            left_mid_tile.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(left_mid_tile, 'tStartRefresh')  # time at next scr refresh
            left_mid_tile.setAutoDraw(True)
        
        # *right_mid_tile* updates
        if right_mid_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
            # keep track of start time/frame for later
            right_mid_tile.frameNStart = frameN  # exact frame index
            right_mid_tile.tStart = t  # local t and not account for scr refresh
            right_mid_tile.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(right_mid_tile, 'tStartRefresh')  # time at next scr refresh
            right_mid_tile.setAutoDraw(True)
        
        # *right_far_tile* updates
        if right_far_tile.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
            # keep track of start time/frame for later
            right_far_tile.frameNStart = frameN  # exact frame index
            right_far_tile.tStart = t  # local t and not account for scr refresh
            right_far_tile.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(right_far_tile, 'tStartRefresh')  # time at next scr refresh
            right_far_tile.setAutoDraw(True)
        
        # *target_image* updates
        if target_image.status == NOT_STARTED and tThisFlip >= 0.7-frameTolerance:
            # keep track of start time/frame for later
            target_image.frameNStart = frameN  # exact frame index
            target_image.tStart = t  # local t and not account for scr refresh
            target_image.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(target_image, 'tStartRefresh')  # time at next scr refresh
            target_image.setAutoDraw(True)
        if target_image.status == STARTED:
            # is it time to stop? (based on global clock, using actual start)
            if tThisFlipGlobal > target_image.tStartRefresh + 0.2-frameTolerance:
                # keep track of stop time/frame for later
                target_image.tStop = t  # not accounting for scr refresh
                target_image.frameNStop = frameN  # exact frame index
                win.timeOnFlip(target_image, 'tStopRefresh')  # time at next scr refresh
                target_image.setAutoDraw(False)
        
        # *keyboard_response* updates
        if keyboard_response.status == NOT_STARTED and t >= 0.1-frameTolerance:
            # keep track of start time/frame for later
            keyboard_response.frameNStart = frameN  # exact frame index
            keyboard_response.tStart = t  # local t and not account for scr refresh
            keyboard_response.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(keyboard_response, 'tStartRefresh')  # time at next scr refresh
            keyboard_response.status = STARTED
            # keyboard checking is just starting
            keyboard_response.clock.reset()  # now t=0
            keyboard_response.clearEvents(eventType='keyboard')
        if keyboard_response.status == STARTED:
            theseKeys = keyboard_response.getKeys(keyList=['b', 'c', 'v'], waitRelease=False)
            _keyboard_response_allKeys.extend(theseKeys)
            if len(_keyboard_response_allKeys):
                keyboard_response.keys = _keyboard_response_allKeys[-1].name  # just the last key pressed
                keyboard_response.rt = _keyboard_response_allKeys[-1].rt
                # was this correct?
                if (keyboard_response.keys == str(corrAns)) or (keyboard_response.keys == corrAns):
                    keyboard_response.corr = 1
                else:
                    keyboard_response.corr = 0
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in trialComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "trial"-------
    for thisComponent in trialComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    trials.addData('square.started', square.tStartRefresh)
    trials.addData('square.stopped', square.tStopRefresh)
    trials.addData('plus.started', plus.tStartRefresh)
    trials.addData('plus.stopped', plus.tStopRefresh)
    trials.addData('cross.started', cross.tStartRefresh)
    trials.addData('cross.stopped', cross.tStopRefresh)
    trials.addData('left_far_tile.started', left_far_tile.tStartRefresh)
    trials.addData('left_far_tile.stopped', left_far_tile.tStopRefresh)
    trials.addData('left_mid_tile.started', left_mid_tile.tStartRefresh)
    trials.addData('left_mid_tile.stopped', left_mid_tile.tStopRefresh)
    trials.addData('right_mid_tile.started', right_mid_tile.tStartRefresh)
    trials.addData('right_mid_tile.stopped', right_mid_tile.tStopRefresh)
    trials.addData('right_far_tile.started', right_far_tile.tStartRefresh)
    trials.addData('right_far_tile.stopped', right_far_tile.tStopRefresh)
    trials.addData('target_image.started', target_image.tStartRefresh)
    trials.addData('target_image.stopped', target_image.tStopRefresh)
    # check responses
    if keyboard_response.keys in ['', [], None]:  # No response was made
        keyboard_response.keys = None
        # was no response the correct answer?!
        if str(corrAns).lower() == 'none':
           keyboard_response.corr = 1;  # correct non-response
        else:
           keyboard_response.corr = 0;  # failed to respond (incorrectly)
    # store data for trials (TrialHandler)
    trials.addData('keyboard_response.keys',keyboard_response.keys)
    trials.addData('keyboard_response.corr', keyboard_response.corr)
    if keyboard_response.keys != None:  # we had a response
        trials.addData('keyboard_response.rt', keyboard_response.rt)
    #this code is to record the reaction times and accuracy of the trial
    
    thisRoutineDuration = t # how long did this trial last
    
    # keyboard_response.rt is the time with which a key was pressed
    # thisRecRT - how long it took for participants to respond after onset of target_image
    # thisAcc - whether or not the response was correct
    
    # compute RT based on onset of target
    thisRecRT = keyboard_response.rt - onset_trial 
    
    # check of the response was correct
    if keyboard_response.corr == 1: 
    
        # if it was correct, assign 'correct' value
        thisAcc = 'correct' 
    
    # if not, assign 'incorrect' value
    else:
        thisAcc = 'incorrect'
    
    # record the actual response times of each trial 
    thisExp.addData('trialRespTimes', thisRecRT) 
    # the Routine "trial" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    thisExp.nextEntry()
    
# completed 1.0 repeats of 'trials'


# ------Prepare to start Routine "end_of_exp"-------
continueRoutine = True
# update component parameters for each repeat
key_resp_2.keys = []
key_resp_2.rt = []
_key_resp_2_allKeys = []
# keep track of which components have finished
end_of_expComponents = [text_2, key_resp_2]
for thisComponent in end_of_expComponents:
    thisComponent.tStart = None
    thisComponent.tStop = None
    thisComponent.tStartRefresh = None
    thisComponent.tStopRefresh = None
    if hasattr(thisComponent, 'status'):
        thisComponent.status = NOT_STARTED
# reset timers
t = 0
_timeToFirstFrame = win.getFutureFlipTime(clock="now")
end_of_expClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
frameN = -1

# -------Run Routine "end_of_exp"-------
while continueRoutine:
    # get current time
    t = end_of_expClock.getTime()
    tThisFlip = win.getFutureFlipTime(clock=end_of_expClock)
    tThisFlipGlobal = win.getFutureFlipTime(clock=None)
    frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
    # update/draw components on each frame
    
    # *text_2* updates
    if text_2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        text_2.frameNStart = frameN  # exact frame index
        text_2.tStart = t  # local t and not account for scr refresh
        text_2.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(text_2, 'tStartRefresh')  # time at next scr refresh
        text_2.setAutoDraw(True)
    
    # *key_resp_2* updates
    waitOnFlip = False
    if key_resp_2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
        # keep track of start time/frame for later
        key_resp_2.frameNStart = frameN  # exact frame index
        key_resp_2.tStart = t  # local t and not account for scr refresh
        key_resp_2.tStartRefresh = tThisFlipGlobal  # on global time
        win.timeOnFlip(key_resp_2, 'tStartRefresh')  # time at next scr refresh
        key_resp_2.status = STARTED
        # keyboard checking is just starting
        waitOnFlip = True
        win.callOnFlip(key_resp_2.clock.reset)  # t=0 on next screen flip
        win.callOnFlip(key_resp_2.clearEvents, eventType='keyboard')  # clear events on next screen flip
    if key_resp_2.status == STARTED and not waitOnFlip:
        theseKeys = key_resp_2.getKeys(keyList=['space'], waitRelease=False)
        _key_resp_2_allKeys.extend(theseKeys)
        if len(_key_resp_2_allKeys):
            key_resp_2.keys = _key_resp_2_allKeys[-1].name  # just the last key pressed
            key_resp_2.rt = _key_resp_2_allKeys[-1].rt
            # a response ends the routine
            continueRoutine = False
    
    # check for quit (typically the Esc key)
    if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
        core.quit()
    
    # check if all components have finished
    if not continueRoutine:  # a component has requested a forced-end of Routine
        break
    continueRoutine = False  # will revert to True if at least one component still running
    for thisComponent in end_of_expComponents:
        if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
            continueRoutine = True
            break  # at least one component has not yet finished
    
    # refresh the screen
    if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
        win.flip()

# -------Ending Routine "end_of_exp"-------
for thisComponent in end_of_expComponents:
    if hasattr(thisComponent, "setAutoDraw"):
        thisComponent.setAutoDraw(False)
thisExp.addData('text_2.started', text_2.tStartRefresh)
thisExp.addData('text_2.stopped', text_2.tStopRefresh)
# check responses
if key_resp_2.keys in ['', [], None]:  # No response was made
    key_resp_2.keys = None
thisExp.addData('key_resp_2.keys',key_resp_2.keys)
if key_resp_2.keys != None:  # we had a response
    thisExp.addData('key_resp_2.rt', key_resp_2.rt)
thisExp.addData('key_resp_2.started', key_resp_2.tStartRefresh)
thisExp.addData('key_resp_2.stopped', key_resp_2.tStopRefresh)
thisExp.nextEntry()
# the Routine "end_of_exp" was not non-slip safe, so reset the non-slip timer
routineTimer.reset()

# Flip one final time so any remaining win.callOnFlip() 
# and win.timeOnFlip() tasks get executed before quitting
win.flip()

# these shouldn't be strictly necessary (should auto-save)
thisExp.saveAsWideText(filename+'.csv', delim='auto')
thisExp.saveAsPickle(filename)
logging.flush()
# make sure everything is closed down
thisExp.abort()  # or data files will save again on exit
win.close()
core.quit()
